<?php

declare(strict_types=1);

use ShenerCloud\Mqtt\DataTypes\ClientId;
use ShenerCloud\Mqtt\DataTypes\Message;
use ShenerCloud\Mqtt\Client;
use ShenerCloud\Mqtt\DataTypes\TopicName;
use ShenerCloud\Mqtt\Protocol\Connect;
use ShenerCloud\Mqtt\Protocol\Connect\Parameters;
use ShenerCloud\Mqtt\Protocol\Publish;

include __DIR__ . '/00.basics.php';

$connect = new Connect();
$connect->setConnectionParameters(new Parameters(new ClientId(basename(__FILE__))));

$client = new Client();
$client->processObject($connect);

$now = new \DateTimeImmutable('now');

// Perform the following actions only if we are connected to the broker
if ($client->isConnected()) {
    // Set the payload to an empty message (this will signal the broker to unset the retained message)
    $message = new Message('', new TopicName(COMMON_TOPICNAME));
    // Set the retain flag to true
    $message->setRetainFlag(true);

    // Finally, make a Publish object
    $publish = new Publish();
    // Set the message
    $publish->setMessage($message);
    // And publish the object to the broker
    $client->processObject($publish);
    printf('Cleared retained message on topic "%s"', COMMON_TOPICNAME);
}
echo PHP_EOL;
